locals {
  tags_module = {
    Terraform               = true
    Terraform_Module        = "terraform-aws-network-tagging"
    Terraform_Module_Source = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/network/terraform-aws-network-tagging.git"
  }
  tags = merge(local.tags_module, var.tags)
}
