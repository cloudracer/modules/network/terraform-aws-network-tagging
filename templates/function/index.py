#!/usr/bin/python3

# Test if code is build or not...

#################################
# - Imports & Global Variables- #
#################################

import argparse  # Argument parser
import sys  # OS System Service
import boto3  # AWS SDK
import json  # To work with JSON data
import os  # To get the current working directory

clients = {}  # Holds all AWS Clients

total_source_resources_found = 0
total_target_resources_found = 0
get_data_errors = 0
matching_resources = 0
resources_tagged = 0
resources_with_no_tags = 0

##################################
# - Helper Functions & Classes - #
##################################

# Simple Class that creates an own exception


class QuitCodeException(Exception):
    pass


class NoRoleException(Exception):
    pass

# Create AWS Clients to work with the AWS SDK


def createAwsClients(targetrole, region):
    global clients

    print('- Creating AWS Service Clients...')

    # Setup STS Client
    sts_target = boto3.client('sts', region_name=region)

    # Assume Roles in Target Accounts
    try:
        assumed_role_object_target = sts_target.assume_role(RoleArn=targetrole, RoleSessionName='PythonTagNACLs')  # Assume role in target account
    except Exception as e:
        raise QuitCodeException('Could not assume Target Role. Details: {0}'.format(e))

    # Read Role Credentials
    credentials_target = assumed_role_object_target['Credentials']

    # Create EC2 Owner Client
    try:
        clients['ec2_owner'] = boto3.client('ec2', region_name=region)
    except Exception as e:
        raise QuitCodeException('Could not create EC2 Owner Client. Details: {0}'.format(e))

    # Create EC2 Target Client
    try:
        clients['ec2_target'] = boto3.client('ec2',
                                             region_name=region,
                                             aws_access_key_id=credentials_target['AccessKeyId'],
                                             aws_secret_access_key=credentials_target['SecretAccessKey'],
                                             aws_session_token=credentials_target['SessionToken'],
                                             )
    except Exception as e:
        raise QuitCodeException('Could not create EC2 Target Client. Details: {0}'.format(e))

# Iterates over the Resources


def iterateOverResources(resource_target_data, resourceKey, resourceIdKey, resource_owner_data):
    print('- Started Tagging process for: {0}'.format(resourceKey))

    for resource in resource_target_data:
        resource_id = getResourceID(resource, resourceIdKey)
        tags = findMatchingTags(resource_id, resourceIdKey, resource_owner_data)  # Find the Matching Tags
        if(tags is not False):
            tagAWSResource(resource_id, tags, clients['ec2_target'])  # Tags a Resource

# Gets the Resource ID


def getResourceID(resource, id_key):
    print('-- Trying to find the ID for the AWS Resource')

    if(id_key in resource):
        resource_id = resource[id_key]
        print('--- Found Resource ID: {0}'.format(resource_id))
        return resource_id
    else:
        print('--- Error! Resource ID not found in Resource Object! Cannot proceed')
        return None

# Finds matching Tags


def findMatchingTags(resource_id, resource_key, resources_owner_data):
    global matching_resources, resources_with_no_tags

    print("-- Searching for Tags for Resource ID:", resource_id)
    tags = None
    for owner_resource in resources_owner_data:
        if(owner_resource[resource_key] == resource_id):
            matching_resources += 1
            if('Tags' in owner_resource):
                tags = owner_resource['Tags']
                print("--- Found Resource Tags: ", tags)
                return tags
            else:
                print('--- Error! Owner Resource ID {0} has no Tags!'.format(resource_id))
                return None

# Tags an AWS Resource


def tagAWSResource(resource_id, tags, client):
    global resources_tagged, resources_with_no_tags

    print("-- Adding Tags for AWS Resource ID: ", resource_id)
    if((tags is not None) and (tags != "")):
        try:
            client.create_tags(Resources=[resource_id], Tags=tags)
        except Exception as e:
            print('--- Error! Could not Tag AWS Resource: {0} with provided Owner Account Tags {1}! Details: {2}'.format(resource_id, tags, e))
        else:
            resources_tagged += 1
    else:
        print('--- Error! Resource ID {0} has no Tags!'.format(resource_id))
        resources_with_no_tags += 1

# Fetches Resource Data from any given resource (recursive function)


def getResourceData(client, data_key, data_function, next_token=None, round_num=1):
    global get_data_errors, total_source_resources_found, total_target_resources_found

    resource_data = []  # List that holds all resources
    resources = {}  # Object for the response
    if(next_token is None):
        print('- Fetching all Resources from type: {0} from Account: {1} ...'.format(data_key, client))
        function_name = "clients['"+client+"']."+data_function+"()"  # We are constructing our function call here
        try:
            resources = eval(function_name)  # Call the describe function the first time
        except Exception as e:
            print('-- Error! Could not fetch Resources from type: {0} from Account: {1}! Details: {2}'.format(data_key, client, e))
            get_data_errors += 1
    else:  # Next Token, another round
        print('- Fetching all Resources from type: {0} from Account: {1}. Round: {2} ...'.format(data_key, client, round_num))
        function_name = "clients['"+client+"']."+data_function+"(, NextToken='"+next_token+"')"  # We are constructing our function call here
        try:
            resources = eval(function_name)  # Call the describe function another time with the "NextToken"
        except Exception as e:
            print('-- Error! Could not fetch Resources from type: {0} from Account: {1}! Details: {2}'.format(data_key, client, e))
            get_data_errors += 1

    if(data_key in resources):
        resource_data.extend(resources[data_key])
    else:
        print('-- Error! No Resources from type: {0} found in Resource object for Account: {1}!'.format(data_key, client))

    if('NextToken' in resources):
        round_num += 1
        resource_data.extend(getResourceData(client, data_key, data_function, next_token=resources['NextToken'], round_num=round_num))

    if(client == 'ec2_owner'):
        total_source_resources_found += len(resource_data)
    else:
        total_target_resources_found += len(resource_data)

    return resource_data

# Loads the JSON config file


def loadJSONFile(file):
    currentDirectory = os.path.dirname(os.path.abspath(__file__))
    total_file_path = currentDirectory+'/'+file
    try:
        f = open(total_file_path, "r")
    except Exception as e:
        print('- Error opening JSON File with all Resource Configs. Cannot proceed! Details: ', e)
        exit()

    try:
        data = json.loads(f.read())
    except Exception as e:
        print('- Error converting contents of JSON File with all Resource Configs to Python object. Cannot proceed! Details: ', e)
        exit()

    f.close()

    if('resources' in data):
        return data['resources']
    else:
        print('- Error parsing JSON File with all Resource Configs. Cannot proceed!')
        exit()

# Tags all Resources in the Target Account


def tagResources():
    resources = loadJSONFile('resources.json')
    print('- Reading all Resource information from the Source and Target Accounts...')
    for resource in resources:
        # Receive all necessary data
        resource_source_data = getResourceData('ec2_owner', resource['data_key'], resource['function_name'])
        resource_target_data = getResourceData('ec2_target', resource['data_key'], resource['function_name'])
        # Loop over all AWS Resources
        iterateOverResources(resource_target_data, resource['data_key'], resource['id_key'], resource_source_data)

# Get the target accounts


def getTargetRoles(sharearn):
    rolename = os.environ['LAMBDA_TAGGING_ROLE_NAME']
    client = boto3.client('ram')

    response = client.get_resource_share_associations(
        associationType='PRINCIPAL',
        resourceShareArns=[
            sharearn
        ]
    )

    roles = []
    # Receive all principals of the resourceShare
    for account in response["resourceShareAssociations"]:
        role = 'arn:aws:iam::' + account['associatedEntity'] + ':role/' + rolename
        roles.append(role)

    return roles

# Do tagging for all accounts in the organization after being manually invoked


def manuallyInvoked():

    # Get all accounts
    try:
        accounts = json.loads(os.environ['ACCOUNTS'])
    except QuitCodeException as e:
        print("- Error: ", e)
        exit()

    # Iterate over all accounts and all regions
    regions = json.loads(os.environ['MANUALLY_REGIONS'])
    rolename = os.environ['LAMBDA_TAGGING_ROLE_NAME']
    for account in accounts:
        print('Account: ', account)
        targetrole = 'arn:aws:iam::' + str(account) + ':role/' + rolename
        for region in regions:
            print('Region: ', region)
            try:
                createAwsClients(targetrole, region)
                tagResources()  # Tag all the AWS Resources

                print('\nFinished tagging for one Account.')
                print('- Target Account: ', targetrole.split(':')[4])
            except QuitCodeException as e:
                print('- No LambdaTaggingRole found in Targetaccount. Skipping..: ')

############
# - MAIN - #
############


def lambda_handler(event, context):
    print('Tagging Script started...\n')
    print(event)

    if "manually" in event:
        print('Manually invoked or by daily trigger. Start tagging for all accounts...\n')
        manuallyInvoked()
    else:
        print('Invoked by shared resource event. Start tagging for the specific shared resource ARN...\n')

        try:
            sharearn = event[0]
            targetroles = getTargetRoles(sharearn)
            region = sharearn.split(':')[3]

        except QuitCodeException as e:
            print("- Error: ", e)
            exit()

        try:
            for targetrole in targetroles:
                createAwsClients(targetrole, region)

                tagResources()  # Tag all the AWS Resources

                print('\nFinished tagging for one principal.')
                print('- Target Account: ', targetrole.split(':')[4])

        except QuitCodeException as e:
            print("- Error: ", e)
            exit()

    print('\nAll Resources tagged! Results:')
    print('- Total Source Resources: ', total_source_resources_found)
    print('- Total Target Resources: ', total_target_resources_found)
    print('- Errors retrieving Resources: ', get_data_errors)
    print('- Matching Source & Target Resources: ', matching_resources)
    print('- Resources Tagged: ', resources_tagged)
    print('- Resources with no Tags: ', resources_with_no_tags)

    return "Finished!"
