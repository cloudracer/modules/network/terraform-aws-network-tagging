locals {
  lambda_manually_regions  = var.account_regions # jsonencode(["eu-central-1", "eu-west-1"])
  lambda_manually_accounts = var.account_ids     # "[950314965724,299553978059,301760661488,641111713321]"
}

# Tagging Lambda
module "lambda_network_tagging" {
  count  = var.create_lambda ? 1 : 0
  source = "../terraform-aws-lambda-autopackage"
  providers = {
    aws = aws.network
  }

  name        = "lambda-network-tagging-vpc-tflz"
  description = "Lambda Function to add missing tags to shared network resources in all principal accounts of a Resource Share"
  handler     = "index.lambda_handler"
  policy = templatefile("${path.module}/templates/iam/permissions.json", {
    NETWORK_ACCOUNT_ID       = data.aws_caller_identity.network.account_id
    LAMBDA_TAGGING_ROLE_NAME = local.lambda_tagging_role_name

  })
  env_vars = {
    LAMBDA_TAGGING_ROLE_NAME = local.lambda_tagging_role_name,
    MANUALLY_REGIONS         = local.lambda_manually_regions,
    ACCOUNTS                 = local.lambda_manually_accounts
  }

  xray_enabled = false

  build_code  = var.build_code_from_source
  code_source = "${path.module}/templates/function"
  code_output = "${path.module}/templates/out"
  runtime     = "python3.8"
  timeout     = 60 * 5
  memory_size = 256

  tags = local.tags
}

# EventBridge Trigger for Lambda
resource "aws_cloudwatch_event_rule" "share_resource_event" {
  count    = var.create_lambda ? 1 : 0
  provider = aws.network

  name        = "event-capture-ram-network-share"
  description = "Capture each RAM Network Resource Share"

  event_pattern = <<EOF
{
  "source": ["aws.ram"],
  "detail-type": ["Resource Sharing State Change"],
  "detail": {
    "status": ["associated"],
    "event": ["Resource Share Association"]
  }
}
EOF
}

resource "aws_cloudwatch_event_target" "lambda" {
  count    = var.create_lambda ? 1 : 0
  provider = aws.network

  rule       = aws_cloudwatch_event_rule.share_resource_event[0].name
  target_id  = "lambda-network-tagging-vpc"
  arn        = module.lambda_network_tagging[0].arn
  input_path = "$.resources"

  depends_on = [
    module.lambda_network_tagging[0]
  ]
}

resource "aws_lambda_permission" "allow_eventbridge_share_resource" {
  count    = var.create_lambda ? 1 : 0
  provider = aws.network

  statement_id  = "AllowExecutionFromEventBridge_ShareResourceTrigger"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_network_tagging[0].arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.share_resource_event[0].arn

  depends_on = [
    module.lambda_network_tagging[0]
  ]
}

# Daily Trigger for Lambda
resource "aws_cloudwatch_event_rule" "daily_trigger_event" {
  count    = var.create_lambda ? 1 : 0
  provider = aws.network

  name        = "event-daily-trigger-tagging-lambda"
  description = "Trigger Tagging Lambda once a day"

  schedule_expression = "rate(1 day)"
}

resource "aws_cloudwatch_event_target" "lambda_daily_trigger" {
  count    = var.create_lambda ? 1 : 0
  provider = aws.network

  rule      = aws_cloudwatch_event_rule.daily_trigger_event[0].name
  target_id = "lambda-network-tagging-vpc-daily-trigger"
  arn       = module.lambda_network_tagging[0].arn
  input = jsonencode({
    "manually" = "true"
  })
  depends_on = [
    module.lambda_network_tagging[0]
  ]
}

resource "aws_lambda_permission" "allow_eventbridge_daily_trigger" {
  count    = var.create_lambda ? 1 : 0
  provider = aws.network

  statement_id  = "AllowExecutionFromEventBridge_DailyTrigger"
  action        = "lambda:InvokeFunction"
  function_name = module.lambda_network_tagging[0].arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.daily_trigger_event[0].arn

  depends_on = [
    module.lambda_network_tagging[0]
  ]
}
