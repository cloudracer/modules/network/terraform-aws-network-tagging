variable "account_ids" {
  type        = string
  description = "As string formatted list of AWS Account IDs"
  default     = "[]"
}

variable "account_regions" {
  type        = string
  description = "As string formatted list of VPC Regions"
  default     = "[]"
}

# Flags #
variable "create_lambda" {
  type        = bool
  description = "If the Lambda Function should be created or not"
  default     = false
}

variable "create_iam_role" {
  type        = bool
  description = "If Network Role should be created or not"
  default     = false
}

variable "build_code_from_source" {
  type        = bool
  description = "If the Lambda tagging code should be build from source or not (needs python 3 installed and correct credentials)"
  default     = false
}

# Other #
variable "tags" {
  description = "Tags that should be applied to all resources"
}
