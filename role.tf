locals {
  lambda_tagging_role_name = "rl-lambda-network-tagging-tflz"
}

# Role needs to be assumed by Network Account
resource "aws_iam_role" "lambda_tagging_role" {
  count    = var.create_iam_role ? 1 : 0
  provider = aws

  name = local.lambda_tagging_role_name

  inline_policy {
    name = "ListSharedResourcesAndTags"

    policy = jsonencode({
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "VisualEditor0",
          "Effect" : "Allow",
          "Action" : [
            "ec2:DescribeInternetGateways",
            "ec2:DeleteTags",
            "ec2:DescribeNetworkInterfaces",
            "ec2:DescribeVpcs",
            "ec2:CreateTags",
            "ec2:DescribeDhcpOptions",
            "ec2:DescribeNatGateways",
            "ec2:DescribeSubnets",
            "ec2:DescribeNetworkAcls",
            "ec2:DescribeRouteTables",
            "ec2:DescribeSecurityGroups",
            "ec2:DescribeEgressOnlyInternetGateways"
          ],
          "Resource" : "*"
        }
      ]
    })
  }
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        ## Account ID of shared resources owner
        Principal = {
          "AWS" = "arn:aws:iam::${data.aws_caller_identity.network.account_id}:root"
        }
      }
    ]
  })

  tags = local.tags
}
