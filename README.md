# Terraform AWS Network Tagging Module

## Module

### Create the Lambda in the Networking Account

```
module "lambda_network_tagging" {
  source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/network/terraform-aws-network-tagging"
  providers = {
    aws = aws.network
    aws.network = aws.network
  }

  create_lambda = true
  create_iam_role = false

  tags = local.tags
}
```

### Create the needed roles in all other Accounts

```
module "lambda_network_tagging" {
  source = "git@gitlab.com:tecracer-intern/terraform-landingzone/modules/network/terraform-aws-network-tagging"
  providers = {
    aws = aws.account
    aws.network = aws.network
  }

  create_lambda = false
  create_iam_role = true

  tags = local.tags
}
```

## Contributing
This module is intended to be a shared module.
Please don't commit any customer-specific configuration into this module and keep in mind that changes could affect other projects.

For new features please create a branch and submit a pull request to the maintainers.

Accepted new features will be published within a new release/tag.

## Pre-Commit Hooks

### Enabled hooks
- id: end-of-file-fixer
- id: trailing-whitespace
- id: terraform_fmt
- id: terraform_docs
- id: terraform_validate
- id: terraform_tflint

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.2.0 |
| <a name="provider_aws.network"></a> [aws.network](#provider\_aws.network) | 4.2.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_lambda_network_tagging"></a> [lambda\_network\_tagging](#module\_lambda\_network\_tagging) | ../terraform-aws-lambda-autopackage | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_event_rule.daily_trigger_event](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_rule.share_resource_event](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_rule) | resource |
| [aws_cloudwatch_event_target.lambda](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_cloudwatch_event_target.lambda_daily_trigger](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_event_target) | resource |
| [aws_iam_role.lambda_tagging_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_lambda_permission.allow_eventbridge_daily_trigger](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_lambda_permission.allow_eventbridge_share_resource](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_permission) | resource |
| [aws_caller_identity.network](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_account_ids"></a> [account\_ids](#input\_account\_ids) | As string formatted list of AWS Account IDs | `string` | `"[]"` | no |
| <a name="input_account_regions"></a> [account\_regions](#input\_account\_regions) | As string formatted list of VPC Regions | `string` | `"[]"` | no |
| <a name="input_build_code_from_source"></a> [build\_code\_from\_source](#input\_build\_code\_from\_source) | If the Lambda tagging code should be build from source or not (needs python 3 installed and correct credentials) | `bool` | `false` | no |
| <a name="input_create_iam_role"></a> [create\_iam\_role](#input\_create\_iam\_role) | If Network Role should be created or not | `bool` | `false` | no |
| <a name="input_create_lambda"></a> [create\_lambda](#input\_create\_lambda) | If the Lambda Function should be created or not | `bool` | `false` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that should be applied to all resources | `any` | n/a | yes |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
